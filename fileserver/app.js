var express = require("express");
var path = require("path");
var fs = require("fs");
var app = express();

app.use((req, res, next) => {
    console.log("IP Petición " + req.url);
    console.log("Fecha Pertición " + new Date());
    next();
});
app.use((req, res, next) => {
    var filePath = path.join(__dirname, "files", req.url);
    fs.stat(filePath, function(err, fileInfo) {
        if (err) {
            next();
            return;
        }
        if (fileInfo.isFile()) {
            res.sendFile(filePath);
        } else {
            next();
        }
    });
});
app.use((req, res) => {
    res.status(404);
    res.send("Es archivo no se encontró");
});
app.listen(3000, () =>
    console.log("La aplicación inició en el puerto 3000")
);