var mongoose = require('mongoose');

var modelSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "El nombre es obligatorio"],
        minlength: [6, "El nombre es demasiado corto"],
        maxlength: [12, "El nombre es muy largo"]
    },
    email: {
        type: String,
        required: [true, "El correo es obligatorio"]
    },
    type: {
        type: String,
        enum: ["Alumno", "Maestro"]
    }
});

var Zombie = mongoose.model("Zombie", modelSchema);
module.exports = Zombie;