let express = require('express');
let router = express.Router();

let Zombie = require('../models/zombie');

router.get('/zombies', (req, res) => {
    Zombie.find().exec((error, zombies) => {
        if (!error) {
            res.status(200).json(zombies);
        } else {
            res.status(500).json(error);
        }
    });
});

module.exports = router;