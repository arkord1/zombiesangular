var express = require('express');
var router = express.Router();

var Zombie = require("../models/zombie");

/* GET home page. */
router.get('/', function(req, res, next) {
    Zombie.find().exec(function(error, zombies) {
        if (!error) {
            console.log(zombies);
            res.render('index', { title: 'Octavio', curso: 'Programación c/s', viewZombies: zombies });
        }
    });

});

router.get('/zombies/add', function(req, res) {
    res.render('add');
});

router.post('/zombies/new', function(req, res) {
    var data = req.body;

    var nuevoZombie = new Zombie({
        name: data.name,
        email: data.email,
        type: data.type
    });

    nuevoZombie.save(function(error) {
        if (error) {
            mensaje = error.errors.name.message
            res.render('add', { mensajeError: mensaje });

        } else {
            res.render('add', { mensajeExito: "Se creó un nuevo zombie!" });
        }
    });

});
router.get('/zombies/edit/:id', async function(req, res) {
    var zombie = await Zombie.findById(req.params.id);
    res.render('edit', { zombie, zombie });

});

router.put('/zombies/edit/:id', async function(req, res) {
    try {
        var zombie = await Zombie.findById(req.params.id);
        zombie.name = req.body.name;
        zombie.email = req.body.email;
        zombie.type = req.body.type;

        await zombie.save();
        res.redirect('/');
    } catch (e) {
        res.render('edit', { zombie: zombie });
    }
});

router.get('/zombies/delete/:id', async function(req, res) {
    var zombie = await Zombie.findById(req.params.id);
    res.render('delete', { zombie: zombie });
});

router.delete('/zombies/delete/:id', async function(req, res) {
    var zombie = await Zombie.findById(req.params.id);

    try {
        zombie.remove();
        res.redirect('/');
    } catch (e) {

    }
});

module.exports = router;