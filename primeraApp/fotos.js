var express = require("express");
var path = require("path");
var fs = require("fs");

var app = express();

// Aqui van los middleware
app.use((req, res, next) => {
    console.log("Dirección IP del cliente: " + req.url);
    console.log("Fecha de acceso: " + new Date());
    next();
});

app.use((req, res, next) => {
    var filePath = path.join(__dirname, "files", req.url);
    fs.stat(filePath, function(err, fileInfo) {
        if (err) {
            next();
            return;
        }
        if (fileInfo.isFile()) {
            res.sendFile(filePath);
        } else {
            next();
        }
    });
});

app.use((req, res) => {
    res.status(404);
    res.send("El archivo no se encontró en el servidor");
});

app.listen(3000, () =>
    console.log("La app de fotos inició en el puerto 3000")
);