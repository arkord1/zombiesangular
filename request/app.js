var express = require('express');
var app = express();

app.use((request, response) => {
    console.log('Petición de la ruta: ' + request.url);
    response.send("<h1>Visitando la ruta " + request.url + "</h1>");
});

app.listen(3000, () =>
    console.log("Iniciando por el puerto 3000")
);