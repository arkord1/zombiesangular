var express = require("express");

var api = express.Router();

api.get("/zonahoraria", (req, res) => {
    res.send("Esta es la versión 2 de mi API, con más características!");
});

api.get("/zonahoraria/error", (req, res) => {
    res.status(500).json({
        error: 500,
        message: "Error Interno del Servidor!"
    });
});

api.get("/zonahoraria/missing", (req, res) => {
    res.status(404).json({
        error: 404,
        message: "No se encontró el recurso solicitado!"
    });
});

module.exports = api;