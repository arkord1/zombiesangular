var express = require("express");

var apiV1 = require("./api/v1.js");
var apiV2 = require("./api/v2.js");

var app = express();

app.use("/v1", apiV1);
app.use("/v2", apiV2);

app.listen(3000, () => {
    console.log("La aplicación está corriendo en el puerto 3000");
});