$(function() {

    var $button = $('#aceptar');
    var $input = $('#cp');

    var $container = $('h1');

    $button.on("click", function(event) {
        event.preventDefault();
        var codigopostal = $input.val();

        var request = $.ajax({
            url: "/" + codigopostal,
            dataType: "json"
        });

        request.done(function(data) {
            $container.text("La temperatura es : " + data.temperature + "°");
        });

        request.fail(function(error) {
            $container.text("Ocurrió un error al tratar de obtener los datos del API. :(");
            console.log(error);
        });

    });

});