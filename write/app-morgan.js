var express = require('express');
var logger = require('morgan');
var http = require('http');

var app = express();
app.use(logger('short'));

app.use((request, response) => {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Hola Mundo!');
});

http.createServer(app).listen(3000);