var express = require('express');
var http = require('http');
var path = require('path');

var app = express();

app.set('views', path.resolve(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get("/", (request, response) => {
    response.render('index', {
        mensaje: 'Bienvenidos a mi vista principal!'
    })
});

http.createServer(app).listen(3000);