var express = require('express');
var http = require('http');
var app = express();

app.use((request, response, next) => {
    console.log('Viene una petición ' + request.method + ' a ' + request.url);
    next();
});

app.use((request, response, next) => {
    var minute = (new Date()).getMinutes();
    if ((minute % 2) === 1) {
        next();
    } else {
        response.statusCode = 403;
        response.end('Forbidden');
    }
});

app.use((request, response) => {
    response.writeHead(200, { 'Content-type': 'text/plain' });
    response.end('Hola mundo');
});

http.createServer(app).listen(3000);