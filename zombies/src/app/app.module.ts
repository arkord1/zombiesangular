import { SettingsService } from './services/settings.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ZombiesComponent } from './pages/zombies/zombies.component';
import { ZombiesModalsComponent } from './modals/zombies/zombiesmodals.component';
import { LoginComponent } from './login/login.component';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HeaderComponent } from './shared/header/header.component';
import { SidemenuComponent } from './shared/sidemenu/sidemenu.component';
import { TitleComponent } from './shared/title/title.component';
import { ProgressComponent } from './pages/progress/progress.component';
import { GraphsComponent } from './pages/graphs/graphs.component';
import { SettingsComponent } from './shared/settings/settings.component';
import { CerebrosComponent } from './pages/cerebros/cerebros.component';
import { RegisterComponent } from './register/register.component';
import { appRouting } from './app.routes';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ZombiesComponent,
    ZombiesModalsComponent,
    LoginComponent,
    NopagefoundComponent,
    DashboardComponent,
    HeaderComponent,
    SidemenuComponent,
    TitleComponent,
    ProgressComponent,
    GraphsComponent,
    SettingsComponent,
    CerebrosComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    appRouting
  ],
  providers: [SettingsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
