import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'modal-zombies',
  templateUrl: './zombiesmodals.component.html',
  styles: ['./zombiesmodals.component.css']
})
export class ZombiesModalsComponent implements OnInit {
  @ViewChild('modal') public modal: ElementRef;

  nombre: string;
  email: string;
  tipo: string;
  error: string;

  constructor(private dataService: DataService, private _renderer: Renderer2) { }

  ngOnInit(): void {
    
  }

  agregarZombie() {
    this.dataService.agregarZombie(this.nombre, this.email, this.tipo)
    .subscribe(
      (resultado) => {
      console.log(resultado);
      this._renderer.selectRootElement(this.modal.nativeElement, true ).click();
      this.dataService.obtenerZombies();
    },(error) => {
      console.log(error);
    })
  }

}
