import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ZombiesModalsComponent } from 'src/app/modals/zombies/zombiesmodals.component';

@Component({
  selector: 'app-zombies',
  templateUrl: './zombies.component.html',
  styleUrls: ['./zombies.component.css']
})
export class ZombiesComponent implements OnInit {
  @Output() propagar = new EventEmitter<string>();

  zombies: any;
  constructor(private _dataService: DataService) { }

  ngOnInit(): void {
    console.log("Actualizando tabla...");
    this.actualizarTabla();
  }

  actualizarTabla() {
    this._dataService.zombiesObservable
    .subscribe((resultados) => {
      this.zombies = resultados;
    });
    
    this._dataService.obtenerZombies();
  }

  eliminar(id) {
    this._dataService.eliminarZombie(id)
    .subscribe((resultado) => {
      console.log(resultado);
      this._dataService.obtenerZombies();
    });
    
  }

  actualizar() {
    this.propagar.emit("Hola mundo");
  }

}
