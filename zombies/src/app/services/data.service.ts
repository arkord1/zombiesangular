import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject, Observable } from 'rxjs';

let apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private updateZombies$ = new Subject<any>();
  zombiesObservable = this.updateZombies$.asObservable();

  constructor(private _client: HttpClient) { 
    
  }

  async obtenerZombies() {
    let zombies = await this._client.get<any>(apiUrl + 'zombies');
    console.log(zombies);
    return this.updateZombies$.next(zombies); 
  }
 
  agregarZombie(nombre: string, email: string, tipo: string) {
    let nuevoZombie = {
      name: nombre,
      email: email,
      type: tipo
    };

    return this._client.post(apiUrl + "zombies/new", nuevoZombie);
  }

  eliminarZombie(id) {
    return this._client.delete(`${apiUrl}zombies/delete/${id}`);
  }

}
